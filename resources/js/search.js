const searchString = document.getElementById('searchString');
const errMsg = document.getElementById('errMsg');
const searchResults = document.getElementById('searchResults');

//search on enter click
const key = (e) => {
    if (e.keyCode == 13) {
        e.preventDefault();
        if (searchString.value.toString().toLowerCase().length >= 3) {
            searchProduct();
        } else {
            errMsg.innerHTML = '<p class="text-danger fw-bold">You have to enter minimum 3 symbols.</p>';
        }
        return false;
    }
};

searchString.addEventListener('keyup', debounce(searchProduct, 1000));
searchString.addEventListener('click', searchProduct);
searchString.addEventListener('keypress', key);

//Get search data from API
function getData(searchString) {
    document.getElementById('spinner').classList.remove("d-none");
    fetch('https://dummyjson.com/products/search?q=' + searchString + '&limit=5&delay=100')
        .then(response => response.json())  // convert to json
        .then(json => {
            // console.log(json.products)  //print data to console
            showResults(json.products)
        })
        .catch(err => console.log('Request Failed', err)); // Catch errors
}

//Get search string from input and check its minimum length
async function searchProduct() {
    document.getElementById('productCard').classList.add("d-none");
    errMsg.innerHTML = '';
    document.getElementById('searchLabel').classList.remove("transparent");

    let val = searchString.value.toString().toLowerCase();
    if (val.length >= 3) {
        await getData(val);
    } else {
        searchResults.innerHTML = '';
        searchResults.classList.add("d-none");
        document.getElementById('searchLabel').classList.add("transparent");
    }
}

//Show products search results
function showResults(productsData) {
    document.getElementById('spinner').classList.add("d-none");
    if (productsData.length > 0) {
        searchResults.classList.remove("d-none");
        searchResults.innerHTML = '';

        searchResults.innerHTML += '' +
            '<div id="' + productsData[0]['id'] + '" class="d-flex flex-row mx-3 product first">' +
            '<div class="w-75 py-3 text-start">' + productsData[0]['title'] + '</div>' +
            '<div class="w-25 py-3 text-end fw-bold">' + productsData[0]['price'] + '</div>' +
            '</div>';

        for (let i = 1; i < productsData.length; i++) {
            searchResults.innerHTML += '' +
                '<div id="' + productsData[i]['id'] + '" class="d-flex flex-row mx-3 product splitter">' +
                '<div class="w-75 py-3 text-start">' + productsData[i]['title'] + '</div>' +
                '<div class="w-25 py-3 text-end fw-bold">' + productsData[i]['price'] + '</div>' +
                '</div>';
        }

        //Show selected product
        let products = document.getElementsByClassName('product')
        for (var i = 0; i < products.length; i++) {
            products[i].onclick = function () {
                let id = this.getAttribute('id')
                document.getElementById('productCard').classList.remove("d-none");
                searchResults.classList.add("d-none");

                //Get information about product for product card
                for (let j = 0; j < productsData.length; j++) {
                    if (productsData[j]['id'] == id) {
                        searchString.value = productsData[j]['title'];
                        document.getElementById('productImage').setAttribute('src', productsData[j]['images'][0])
                        document.getElementById('productTitle').textContent = productsData[j]['title'];
                        document.getElementById('productBrand').textContent = productsData[j]['brand'];
                        document.getElementById('productCategory').textContent = productsData[j]['category'];
                        document.getElementById('productPrice').textContent = productsData[j]['price'];
                        document.getElementById('productDescription').textContent = productsData[j]['description'];
                    }
                }
            }
        }
    } else {
        searchResults.classList.add("d-none");
        errMsg.innerHTML = ' <p class="text-danger fw-bold">No data found!</p>';
    }
}

//Wate for typing search string before sending API request
function debounce(callback, wait) {
    let timeout
    return (...args) => {
        clearTimeout(timeout)
        timeout = setTimeout(function () {
            callback.apply(this, args);
        }, wait)
    }
}