let terminalBody, userInput, terminalOutput, terminalPrompt;
let lastCommands = [];

//built-in commands
const COMMANDS = [
    {
        name: 'help',
        msg: 'show commands list;',
    },
    {
        name: 'clear',
        msg: 'clear the terminal;',
    },
    {
        name: 'history',
        msg: 'show history of users prompts;',
    },
    {
        name: 'quote',
        msg: 'show random quote from API;',
    },
    {
        name: 'double',
        msg: 'returns the result of multiplication by 2;',
    },
]

//custom commands
const CUSTOM_COMMANDS = [
    {
        name: 'hello',
        msg: 'Hello :)',
    },
]

//execute command from input
const execute = function executeCommand(input) {
    input = input.toLowerCase();
    lastCommands.push(input);
    let output;
    let value;

    if (input.includes('double')) {
        value = input.substring(7);
        input = 'double';
    }

    const i = COMMANDS.findIndex(e => e.name === input);
    const j = CUSTOM_COMMANDS.findIndex(e => e.name === input);

    if (i > -1 || j > -1) {
        // COMMANDS or CUSTOM_COMMANDS contains the element we're looking for, at index "i"
        switch (input) {
            case 'help':
                showHelp();
                break;
            case 'clear':
                clearScreen();
                break;
            case 'cls':
                clearScreen();
                break;
            case 'history':
                showHistory();
                break;
            case 'quote':
                showQuote();
                break;
            case 'double':
                showDouble(value);
                break;
            default: //show command msg in output
                output = showUserCurrentCommand(input);
                output += '<span class="text-green fw-bold">terminal:</span><span class="mx-2">-></span>';
                if (i > -1) {
                    output += COMMANDS[i].msg;
                } else {
                    output += CUSTOM_COMMANDS[j].msg;
                }
                terminalOutput.innerHTML += '<br/><br/>' + output;
        }
    } else { //command not found
        output = showUserCurrentCommand(input) + notFound(input);
        terminalOutput.innerHTML += '<br/><br/>' + output;
    }

    //autoscroll
    terminalBody.scrollTo(0, terminalBody.scrollHeight);
};

//user's current command for output
function showUserCurrentCommand(input) {
    return '<span class="text-green fw-bold">you:</span><span class="mx-2">~</span>' +
        '<span>' + input + '</span></br>';
}

//command not found Msg
function notFound(input) {
    return '<span class="text-green fw-bold">terminal:</span><span class="mx-2">-></span>' +
        '<span class="text-red">command</span><span class="mx-2">~</span>' + input + '<span' +
        ' class="text-red ms-2">not found!</span>';
}

//clear screen
function clearScreen() {
    location.reload();
}

//show help
function showHelp() {
    let output = showUserCurrentCommand('help');
    output += '<span class="text-green fw-bold">terminal:</span><span class="mx-2">-></span><span>Supported commands:</span>';
    for (let i = 0; i < COMMANDS.length; i++) {
        output += '<div class="row">' +
            '<div class="col-3">~ ' + COMMANDS[i].name + '</div>' +
            '<div class="col-9">' + COMMANDS[i].msg + '</div>' +
            '</div>'
    }
    for (let i = 0; i < CUSTOM_COMMANDS.length; i++) {
        output += '<div class="row">' +
            '<div class="col-3">~ ' + CUSTOM_COMMANDS[i].name + '</div>' +
            '<div class="col-9">' + CUSTOM_COMMANDS[i].msg + '</div>' +
            '</div>'
    }
    output += '<span>Tip: use Up/Down arrow to go through resent commands;</span>';
    terminalOutput.innerHTML += '<br/><br/>' + output;
}

//show history
function showHistory() {
    let output = showUserCurrentCommand('history');
    output += '<span class="text-green fw-bold">terminal:</span><span class="mx-2">-></span>' +
        '<span class="text-orange">' + lastCommands.join(", ") + '</span>';
    terminalOutput.innerHTML += '<br/><br/>' + output;
}

//show random quote from API
async function showQuote() {
    let quote = 'Sorry, no quote found.';
    let author = '';

    await fetch('https://dummyjson.com/quotes/random')
        .then(response => response.json())  // convert to json
        .then(json => {
            //console.log(json); //print data to console
            quote = json.quote;
            author = ' (' + json.author + ')';
        })
        .catch(err => console.log('Request Failed', err)); // Catch errors

    setTimeout(function () {
        let output = showUserCurrentCommand('quote');
        output += '<span class="text-green fw-bold">terminal:</span><span class="mx-2">-></span>' +
            '<span>' + quote + '<i>' + author + '</i></span>';
        terminalOutput.innerHTML += '<br/><br/>' + output;
        terminalBody.scrollTo(0, terminalBody.scrollHeight);
    }, 1000);
}

//returns the result of multiplication by 2
function showDouble(value) {
    let res = '';
    if (value === '') {
        res = '<span class="text-red"> You have to prompt a number after</span>' +
            '<span class="mx-2">double</span><span class="text-red">command!</span> ';
    } else if (!isNaN(value)) {
        res = value + '*2=' + value * 2;
    } else {
        res = value + '<span class="text-red"> is not a number!</span>';
    }
    let output = showUserCurrentCommand('double ' + value);
    output += '<span class="text-green fw-bold">terminal:</span><span class="mx-2">-></span>' +
        '<span>' + res + '</span>';
    terminalOutput.innerHTML += '<br/><br/>' + output;
}

//show command prompts
function commandPrompt() {
    const input = userInput.innerHTML;
    let foundedCommands = [];

    if (input.toString().toLowerCase().length >= 2) {
        for (let i = 0; i < COMMANDS.length; i++) {
            if (COMMANDS[i].name.includes(input)) {
                foundedCommands.push('~' + COMMANDS[i].name);
            }
        }
        for (let i = 0; i < CUSTOM_COMMANDS.length; i++) {
            if (CUSTOM_COMMANDS[i].name.includes(input)) {
                foundedCommands.push('~' + CUSTOM_COMMANDS[i].name);
            }
        }
    }

    terminalPrompt.innerHTML = foundedCommands.join(", ");
}

//set variables for html elements
const app = () => {
    terminalBody = document.getElementById('terminalBody')
    userInput = document.getElementById('userInput');
    terminalPrompt = document.getElementById('terminalPrompt');
    terminalOutput = document.getElementById('terminalOutput');
    document.getElementById('keyboard').focus();
};

//execute command on enter click
const key = (e) => {
    const input = userInput.innerHTML;
    if (e.key === "Enter") {
        execute(input);
        userInput.innerHTML = "";
        return;
    }
    userInput.innerHTML = input + e.key;
};

//edit command - delete symbol
const backspace = (e) => {
    if (e.keyCode !== 8 && e.keyCode !== 46) {
        return;
    }
    userInput.innerHTML = userInput.innerHTML.slice(
        0,
        userInput.innerHTML.length - 1
    );
};

//commands history by arrows
let iter = 0;
const arr = (e) => {
    if (e.key === "ArrowUp") {
        if (lastCommands.length > 0 && iter < lastCommands.length) {
            iter += 1;
            userInput.innerHTML = lastCommands[lastCommands.length - iter];
        }
    }

    if (e.key === "ArrowDown") {
        if (lastCommands.length > 0 && iter > 1) {
            iter -= 1;
            userInput.innerHTML = lastCommands[lastCommands.length - iter];
        }
    }
};

document.addEventListener("DOMContentLoaded", app);
document.addEventListener("keydown", backspace);
document.addEventListener("keypress", key);
document.addEventListener("keydown", arr);
document.getElementById('keyboard').addEventListener('keyup', commandPrompt);

